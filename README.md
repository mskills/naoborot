### naoborot
Навык для маруси повторяет слова наоборот

## Env
|Key|Default|Description|
|---|---|---|
|SKILL_USE_SSL| false | Использовать SSL |
|SKILL_SSL_CERT_FILE | _no_ | Путь к сертификату |
|SKILL_SSL_KEY_FILE | _no_ | Путь к ключу |
|SKILL_ADDR| :9000 | Прослушиваемый порт |
|SKILL_WEBHOOK_URI| /webhook | location |