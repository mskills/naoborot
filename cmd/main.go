package main

import (
	"log"
	"naoborot/internal/env"
	"naoborot/internal/game"
	"strconv"

	"github.com/DesSolo/marusia"
	"github.com/DesSolo/marusia/middlewares"
)

func loadConfig() (*marusia.Config, error) {
	useSSL, err := strconv.ParseBool(env.GetEnvOrDefault("SKILL_USE_SSL", "false"))
	if err != nil {
		return nil, err
	}

	var certFile, keyFile, addr, webhookURL string

	if useSSL {
		certFile = env.GetEnvOrFault("SKILL_SSL_CERT_FILE")
		keyFile = env.GetEnvOrFault("SKILL_SSL_KEY_FILE")
	}

	addr = env.GetEnvOrDefault("SKILL_ADDR", ":9000")
	webhookURL = env.GetEnvOrDefault("SKILL_WEBHOOK_URI", "/webhook")

	return marusia.NewConfig(useSSL, certFile, keyFile, addr, webhookURL), nil
}

func main() {

	r := marusia.NewDialogRouter(true)
	r.RegisterDefault(game.Default)

	r.Use(middlewares.Logging)
	r.Register("привет", game.Hello)
	r.Register("пока", game.End)

	config, err := loadConfig()
	if err != nil {
		log.Fatal(err)
	}

	skill := marusia.NewSkill(config, r)
	if err := skill.ListenAndServe(); err != nil {
		log.Fatal(err)
	}
}
