package game

import (
	"context"
	"strings"

	"github.com/DesSolo/marusia"
)

func Default(req *marusia.Request, resp *marusia.Response, _ context.Context) {
	src := strings.ToLower(req.OriginalUtterance())
	runes := []rune(src)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}

	message := string(runes)
	resp.Text(message)
	resp.TTS(message)
}

func Hello(req *marusia.Request, resp *marusia.Response, _ context.Context) {
	message := `Я буду говорить задом наоборот все что Вы мне скажете. Скажите "стоп" "хватит" или "пока" чтобы закончить`
	resp.Text(message)
	resp.TTS(message)
}

func End(req *marusia.Request, resp *marusia.Response, _ context.Context) {
	message := `Пока, пока`
	resp.Text(message)
	resp.TTS(message)
	resp.EndSession()
}
