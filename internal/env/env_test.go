package env

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_GetEnvOrFault(t *testing.T) {
	for _, tc := range []struct {
		key, actual, expected string
		hasError              bool
	}{
		{key: "test_1", actual: "val_1", hasError: false},
		{key: "test_2", actual: "", hasError: true},
	} {
		assert.NoError(t, os.Setenv(tc.key, tc.actual))
		if tc.hasError {
			continue
		} else {
			assert.Equal(t, GetEnvOrFault(tc.key), os.Getenv(tc.key))
		}
	}
}

func Test_GetEnvOrDefault(t *testing.T) {
	for _, tc := range []struct {
		key, def, expected, actual string
		isOk                       bool
	}{
		{key: "test_1", def: "", expected: "", actual: "", isOk: true},
		{key: "test_2", def: "val_2", expected: "val_2", actual: "val_2", isOk: true},
		{key: "test_3", def: "", expected: "val_3", actual: "val_3", isOk: true},
		{key: "test_4", def: "", expected: "", actual: "val_4", isOk: false},
	} {
		assert.NoError(t, os.Setenv(tc.key, tc.actual))
		val := GetEnvOrDefault(tc.key, tc.def)
		if tc.isOk {
			assert.Equal(t, val, tc.expected)
		} else {
			assert.NotEqual(t, val, tc.expected)
		}
	}
}
