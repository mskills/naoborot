package env

import (
	"log"
	"os"
)

// GetEnvOrFault get value from env or exit
func GetEnvOrFault(kev string) string {
	val := os.Getenv(kev)
	if val == "" {
		log.Fatalf("env \"%s\" not set", kev)
	}

	return val
}

// GetEnvOrDefault get value from env or default
func GetEnvOrDefault(kev, def string) string {
	val := os.Getenv(kev)
	if val == "" {
		return def
	}

	return val
}
